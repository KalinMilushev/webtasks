var geocoder;
var map;



function initMap() {
    geocoder = new google.maps.Geocoder();
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: {
            lat: 42.69,
            lng: 23.32
        }
    });

    alert("InitMap function is running");
    document.getElementById('mapsForm').addEventListener('submit', function() {
        alert("Submit is clicked");
        geocodeAddress();

    });
}



function geocodeAddress() {
    var address = document.getElementById('address').value;

    alert("Address from the form is " + address);

    geocoder.geocode({
        'address': address
    }, function(results, status) {
        if (status == 'OK') {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}